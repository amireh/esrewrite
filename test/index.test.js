const fs = require('fs')
const path = require('path')
const rewrite = require('../lib')
const sinon = require('sinon')
const { assert } = require('chai')

sinon.assert.expose(assert, { prefix: "" });

describe('rewrite', () => {
  it('does nothing when asked to', () => {
    const graph = { dependencies: [], dependents: [] }
    const renames = []

    rewrite(renames, graph, {})
  })

  it('mods a file', () => {
    const graph = {
      dependents: {
        'fixture/a.js': [],
        'fixture/b.js': [{ id: 'fixture/a.js', userRequest: './b' }],
      },
      dependencies: {
        'fixture/a.js': [{ id: 'fixture/b.js', userRequest: './b' }],
        'fixture/b.js': [],
      },
    }

    const renames = [
      ['fixture/b.js', 'fixture/c.js']
    ]

    const parser = { parse: sinon.stub(), walk: sinon.stub(), }
    const mod = sinon.stub()

    const stats = rewrite(renames, graph, {
      context: __dirname,
      dry: true,
      parsers: [
        {
          when: () => true,
          use: parser
        }
      ],
      mods: [
        {
          when: () => true,
          apply: mod
        }
      ]
    })

    assert.empty(stats.warnings)
    assert.called(parser.parse)
  })
})
