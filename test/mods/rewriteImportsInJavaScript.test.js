const mod = require('../../lib/mods/rewriteImportsInJavaScript')
const { assert } = require('chai')
const dedent = require('dedent')
const { parse, walk } = require('../../lib/parsers/javascript')

describe('mods/rewriteImportsInJavaScript', () => {
  const specs = [
    {
      source: `import "foo"`,
      modded: `import "bar"`,
      requests: {
        'foo': 'bar'
      }
    },

    {
      source: `import * as Foo from "foo"`,
      modded: `import * as Foo from "bar"`,
      requests: {
        'foo': 'bar'
      }
    },

    {
      source: `import { x } from "foo"`,
      modded: `import { x } from "bar"`,
      requests: {
        'foo': 'bar'
      }
    },

    {
      source: `import("foo")`,
      modded: `import("bar")`,
      requests: {
        'foo': 'bar'
      }
    },

    {
      source: 'import "exports-loader?Ember!foo"',
      modded: 'import "exports-loader?Ember!bar"',
      requests: {
        'exports-loader?Ember!foo': 'bar'
      }
    },

    {
      source: 'require("foo")',
      modded: 'require("bar")',
      requests: {
        'foo': 'bar'
      }
    },

    {
      source: 'require(["foo"])',
      modded: 'require(["bar"])',
      requests: {
        'foo': 'bar'
      }
    },
    {
      source: 'require(["a", "foo"])',
      modded: 'require(["a", "bar"])',
      requests: {
        'foo': 'bar'
      }
    },
    {
      source: 'require(["foo", "a"])',
      modded: 'require(["bar", "a"])',
      requests: {
        'foo': 'bar'
      }
    },
    {
      source: 'require(["foo"], function(foo) {})',
      modded: 'require(["bar"], function(foo) {})',
      requests: {
        'foo': 'bar'
      }
    },
  ]

  for (const { source, modded, requests } of specs) {
    it(source, () => {
      const ast = parse(source)
      const stats = { mods: [] }

      mod({
        file: '<stdin>',
        lines: source.split('\n'),
        text: source,
        requests,
        stats,
        walk: fn => walk(ast, fn)
      })

      assert.notEmpty(stats.mods)
      assert.equal(stats.mods[0][2], modded)
    })
  }
})
