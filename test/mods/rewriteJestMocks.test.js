const mod = require('../../lib/mods/rewriteJestMocks')
const { assert } = require('chai')
const dedent = require('dedent')
const { parse, walk } = require('../../lib/parsers/javascript')

describe('mods/rewriteJestMocks', () => {
  const specs = [
    {
      file: '/tmp/test.js',
      source: 'jest.mock("./foo")',
      modded: 'jest.mock("/tmp/bar.js")',
      renamed: {
        '/tmp/foo.js': '/tmp/bar.js'
      },
      originals: {
        '/tmp/test.js': '/tmp/test.js'
      }
    },

    // accepts a custom resolver
    {
      source: 'jest.mock("compiled/foo")',
      modded: 'jest.mock("bar.js")',
      renamed: {
        'foo.js': 'bar.js'
      },
      resolve: request => request === 'compiled/foo' ? 'foo.js' : null,
    },
  ]

  for (const { source, modded, renamed, originals, resolve, ...other } of specs) {
    it(source, () => {
      const ast = parse(source)
      const stats = { mods: [], warnings: [] }

      mod({ resolve })({
        file: '/tmp/test.js',
        lines: source.split('\n'),
        originals,
        renamed,
        stats,
        walk: fn => walk(ast, fn),
        ...other
      })

      assert.deepEqual([], stats.warnings)
      assert.notEmpty(stats.mods)
      assert.equal(stats.mods[0][2], modded)
    })
  }
})
