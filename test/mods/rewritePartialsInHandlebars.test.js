const mod = require('../../lib/mods/rewritePartialsInHandlebars')
const { assert } = require('chai')
const { parse, walk } = require('../../lib/parsers/handlebars')

describe('mods/rewritePartialsInHandlebars', () => {
  const specs = [
    {
      source: `{{> jst/_foo }}`,
      modded: `{{> /path/to/_bar }}`,
      requests: {
        'jst/_foo': '/path/to/_bar'
      }
    },

    // accepts a resolve option
    {
      source: `{{> avatar }}`,
      modded: `{{> /new/path/to/avatar.handlebars }}`,
      requests: {
        'jst/path/to/_avatar.handlebars': '/new/path/to/avatar.handlebars'
      },
      options: {
        resolve: (request) => request === 'avatar' ? 'jst/path/to/_avatar.handlebars' : request
      }
    },
  ]

  for (const { source, modded, requests, options } of specs) {
    it(source, () => {
      const ast = parse(source)
      const stats = { mods: [], warnings: [] }

      mod(options || {})({
        file: '<stdin>',
        lines: source.split('\n'),
        text: source,
        requests,
        stats,
        walk: fn => walk(ast, fn)
      })

      assert.deepEqual([], stats.warnings)
      assert.notEmpty(stats.mods)
      assert.equal(stats.mods[0][2], modded)
    })
  }
})
