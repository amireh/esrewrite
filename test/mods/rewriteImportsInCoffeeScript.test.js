const mod = require('../../lib/mods/rewriteImportsInCoffeeScript')
const { assert } = require('chai')
const { parse, walk } = require('../../lib/parsers/coffeescript')

describe('mods/rewriteImportsInCoffeeScript', () => {
  const specs = [
    {
      source: `import "foo"`,
      modded: `import "bar"`,
      requests: {
        'foo': 'bar'
      }
    },
  ]

  for (const { source, modded, requests } of specs) {
    it(source, () => {
      const ast = parse(source)
      const stats = { mods: [] }

      mod({
        file: '<stdin>',
        lines: source.split('\n'),
        text: source,
        requests,
        stats,
        walk: fn => walk(ast, fn)
      })

      assert.notEmpty(stats.mods)
      assert.equal(stats.mods[0][2], modded)
    })
  }
})
