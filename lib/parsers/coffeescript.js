const walkCoffee = require('coffee-ast-walk')
const coffee = require('coffee-script')

exports.parse = text => coffee.nodes(text)
exports.walk = (ast, fn) => walkCoffee(ast).walk(fn)