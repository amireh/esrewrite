const Handlebars = require('handlebars')
const Visitor = require('handlebars/dist/cjs/handlebars/compiler/visitor')['default'];

exports.parse = text => Handlebars.parse(text)
exports.walk = (ast, fn) => new HandlebarsVisitor(fn).accept(ast);

function HandlebarsVisitor(callback) {
  this.callback = callback
}

HandlebarsVisitor.prototype = new Visitor();
HandlebarsVisitor.prototype.accept = function(object) {
  this.callback(object)
  return this[object.type](object);
};

HandlebarsVisitor.prototype.program = function(program) {
  var i, l;

  for(i=0, l=program.statements.length; i<l; i++) {
    this.accept(program.statements[i]);
  }
};

HandlebarsVisitor.prototype.block = function(block) {
  this.accept(block.mustache);

  if (block.program) {
    this.accept(block.program);
  }

  if (block.inverse) {
    this.accept(block.inverse);
  }
};

HandlebarsVisitor.prototype.sexpr = function(sexpr) {
  var params = sexpr.params;

  for (var i=0, l=params.length; i<l; i++) {
    this.accept(params[i]);
  }

  if (sexpr.hash) {
    this.accept(sexpr.hash);
  }

  return this.accept(sexpr.id);
};

HandlebarsVisitor.prototype.mustache = function(mustache) {
  return this.accept(mustache.sexpr);
};

HandlebarsVisitor.prototype.partial = function(partial) {
  this.accept(partial.partialName);

  if (partial.context) {
    this.accept(partial.context);
  }
};

HandlebarsVisitor.prototype.hash = function(hash) {
  var pairs = hash.pairs;

  for(var i=0, l=pairs.length; i<l; i++) {
    this.accept(pairs[i][1]);
  }
};

HandlebarsVisitor.prototype.STRING = function(string) {
};

HandlebarsVisitor.prototype.INTEGER = function(integer) {
};

HandlebarsVisitor.prototype.BOOLEAN = function(bool) {
};

HandlebarsVisitor.prototype.ID = function(id) {
};

HandlebarsVisitor.prototype.PARTIAL_NAME = function(partialName) {
};

HandlebarsVisitor.prototype.DATA = function(data) {
  return this.accept(data.id);
};

HandlebarsVisitor.prototype.content = function(content) {
};

HandlebarsVisitor.prototype.comment = function(comment) {
};

