const { parseScript } = require('meriyah')
const walk = require('dash-ast')

exports.parse = text => parseScript(text, {
  module: true,
  loc: true,
  jsx: true,
  next: true
})

exports.walk = walk