const rewriteImportsInJavaScript = ({ file, lines, requests, walk, stats }) => {
  const arrayOfSources = x => x && Array.isArray(x) ? x : [].concat(x || [])

  walk((node, parent) => {
    const sources = arrayOfSources(extractImportSourcesInJavaScript(file, node, stats))

    for (const source of sources) {
      if (requests.hasOwnProperty(source.value)) {
        lines[source.loc.start.line - 1] = rewriteValueKeepLoaders(
          lines[source.loc.start.line - 1],
          source.loc.start.column + 1 /* keep quote */,
          source.loc.end.column - 1 /* keep quote */,
          requests[source.value]
        );

        stats.mods.push([file, source.loc.start.line, lines[source.loc.start.line - 1]])
      }
    }
  })

  return lines
}

// we need to preserve loader requests like:
//
//     import "exports-loader?Ember!ember"
//             ^^^^^^^^^^^^^^^^^^^^^
//
// this only occurs in javascript sources so we don't need to do it elsewhere
const rewriteValueKeepLoaders = (line, startColumn, endColumn, request) => {
  const value = line.slice(startColumn, endColumn)
  const requestWithLoaders = value.includes('!') ? (
    value.split('!').slice(0, -1).concat([request]).join('!')
  ) : (
    request
  )

  return (
    line.slice(0, startColumn) +
    requestWithLoaders +
    line.slice(endColumn)
  )
}

const extractImportSourcesInJavaScript = (file, node, stats) => {
  // import 'foo'
  if (node.type === 'ImportDeclaration') {
    if (node.source.type === 'Literal') {
      if (node.source.loc.start.line === node.source.loc.end.line) {
        return node.source
      }
      else {
        stats.warnings.push([file, node.loc.start.line, 'import source spans multiple lines!'])
      }
    }
    else {
      stats.warnings.push([file, node.loc.start.line, 'ImportDeclaration source is non-Literal'])
    }
  }
  // import('foo')
  else if (node.type === 'ImportExpression') {
    if (node.source.type === 'Literal') {
      return node.source
    }
    else {
      stats.warnings.push([file, node.loc.start.line, 'ImportExpression source is non-Literal'])
    }
  }
  // require(...)
  else if (
    node.type === 'CallExpression' &&
    node.callee.type === 'Identifier' &&
    node.callee.name === 'require'
  ) {
    // require('foo')
    if (node.arguments.length === 1 && node.arguments[0].type === 'Literal') {
      return node.arguments[0]
    }
    // require(['foo'])
    else if (node.arguments[0].type === 'ArrayExpression' && node.arguments[0].elements.some(x => x.type === 'Literal')) {
      return node.arguments[0].elements.filter(x => x.type === 'Literal')
    }
    else {
      stats.warnings.push([file, node.loc.start.line, 'require() argument is not supported'])
    }
  }
}

module.exports = rewriteImportsInJavaScript