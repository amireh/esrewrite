module.exports = ({ file, lines, requests, stats, walk }) => {
  walk(node => {
    if (node.constructor.name === 'ImportDeclaration') {
      if (node.source.constructor.name === 'StringLiteral') {
        const request = requests[node.source.value.replace(/^(['"])|['"]$/g, '')]

        if (request) {
          const loc = {
            start: {
              line: node.source.locationData.first_line,
              column: node.source.locationData.first_column,
            },
            end: {
              line: node.source.locationData.last_line,
              column: node.source.locationData.last_column,
            }
          }

          const original = lines[loc.start.line]
          const renamed = (
            original.slice(0, loc.start.column + 1 /* keep quote */) +
            request +
            original.slice(loc.end.column)
          );

          lines[loc.start.line] = renamed

          stats.mods.push([file, loc.start.line, renamed])
        }

      }
      else {
        stats.warnings.push([file, node.locationData.first_line, 'ImportDeclaration source is not a literal'])
      }
    }
  })

  return lines
}
