const path = require('path')
const noop = () => null
const identity = x => x

module.exports = ({ optimize = identity, resolve = noop }) => ({ file, renamed, originals, lines, stats, walk }) => {
  const follow = file => (
    renamed[file] || renamed[`${file}.js`] || renamed[`${file}.coffee`]
  );

  const replace = ({ node, target }) => {
    if (!target) {
      stats.warnings.push([file, node.loc.start.line, `unable to resolve jest.mock("${node.value}")`])
      return
    }

    lines[node.loc.start.line-1] = (
      lines[node.loc.start.line-1].slice(0, node.loc.start.column + 1) +
      optimize(target, file, { request: node.value }) +
      lines[node.loc.start.line-1].slice(node.loc.end.column - 1)
    )

    stats.mods.push([file, node.loc.start.line, lines[node.loc.start.line-1]])
  };

  walk((node, parent) => {
    if (
      node.type === 'CallExpression' &&
      node.callee.type === 'MemberExpression' &&
      node.callee.object.type === 'Identifier' &&
      node.callee.object.name === 'jest' &&
      node.callee.property.type === 'Identifier' &&
      node.callee.property.name === 'mock' &&
      node.arguments.length > 0 &&
      node.arguments[0].type === 'Literal'
    ) {
      const targetNode = node.arguments[0]

      if (targetNode.value.startsWith('.')) {
        replace({
          node: targetNode,
          target: follow(path.resolve(path.dirname(originals[file]), targetNode.value)),
        })
      }
      else {
        const target = resolve(targetNode.value, file)

        if (target) {
          replace({
            node: targetNode,
            target: follow(target)
          })
        }
        else {
          stats.warnings.push([file, targetNode.loc.start.line, `don't know how to rewrite jest.mock("${targetNode.value}")`])
        }
      }
    }
  })

  return lines
}
