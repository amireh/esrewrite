module.exports = ({ relative = Identity, resolve = Identity }) => ({ file, lines, requests, stats, walk }) => {
  walk(node => {
    if (node.type === 'partial') {
      const request = requests[resolve(node.partialName.name)]

      if (request) {
        const loc = {
          start: {
            line: node.partialName.firstLine - 1,
            column: node.partialName.firstColumn,
          },
          end: {
            line: node.partialName.lastLine - 1,
            column: node.partialName.lastColumn,
          }
        }

        const original = lines[loc.start.line]
        const renamed = (
          original.slice(0, loc.start.column) +
          request +
          original.slice(loc.end.column)
        );

        lines[loc.start.line] = renamed

        stats.mods.push([file, loc.start.line, renamed])
      }
      else {
        stats.warnings.push([file, node.partialName.firstLine, `found a partial but no rewrite: ${node.partialName.name}`])
      }
    }
  })

  return lines
}

const Identity = x => x
