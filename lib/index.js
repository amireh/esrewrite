#!/usr/bin/env node

// esrewrite - rewrite specifiers in import and similar statements

const fs = require('fs')
const path = require('path')
const glob = require('glob')
const micromatch = require('micromatch')

const main = (renames, { dependencies, dependents }, {
  context = null,
  dry = false,
  exclude = [],
  ignore = [],
  include = [],
  mods = [],
  optimize = Identity,
  parsers = [],
  stats = { mods: [], warnings: [] },
  additionalRequests = {},
}) => {
  const renamed = indexBy(([oldFile, newFile]) => ([oldFile, newFile]), renames)
  const originals = indexBy(([oldFile, newFile]) => ([newFile, oldFile]), renames)

  // Map.<file: Path, Map.<request: String, target: Path>>
  const rewrites = Object.assign({}, additionalRequests)

  const relative = context ? file => path.relative(context, file) : Identity;
  const follow = file => renamed[file] || file
  const enqueue = (file, request, target) => {
    if (exclude.length && micromatch.isMatch(relative(file), exclude)) {
      return
    }
    else if (include.length && !micromatch.isMatch(relative(file), include)) {
      return
    }

    if (!rewrites[file]) {
      rewrites[file] = {}
    }

    rewrites[file][request] = target
  }

  const transform = (file, requests) => {
    const parserForFile = parsers.find(x => x.when({ file }))

    if (!parserForFile) {
      return null
    }

    const modsForFile = mods.filter(x => x.when({ file }))

    if (!modsForFile.length) {
      return null
    }

    const state = parse(context, file, parserForFile.use)
    const params = {
      file,
      originals,
      renamed,
      requests,
      stats,
    }

    for (const mod of modsForFile) {
      mod.apply({ ...params, ...state })
    }

    return state.lines.join('\n')
  }

  const ignored = ignore.length ?
    file => micromatch.isMatch(relative(file), ignore) :
    () => false
  ;

  for ([oldFile, newFile] of renames) {
    if (dependents.hasOwnProperty(oldFile)) {
      for (const { id: dependent, userRequest } of dependents[oldFile]) {
        enqueue(follow(dependent), userRequest, newFile)
      }
    }
    else if (!ignored(oldFile)) {
      stats.warnings.push(`Module has no dependents: ${oldFile}`)
    }

    if (dependencies.hasOwnProperty(oldFile)) {
      for (const { id: dependency, userRequest } of dependencies[oldFile]) {
        if (renamed.hasOwnProperty(dependency)) {
          enqueue(newFile, userRequest, follow(dependency))
        }
      }
    }
    else if (!ignored(oldFile)) {
      stats.warnings.push(`Module has no dependencies: ${oldFile}`)
    }
  }

  for (const file/*on disk*/ of Object.keys(rewrites)) {
    const optimizedRewrites = Object.keys(rewrites[file]).reduce((acc, source) => {
      acc[source] = optimize(rewrites[file][source], file, { request: source })
      return acc
    }, {})

    const modded = transform(file, optimizedRewrites, renamed, originals)

    if (modded && !dry) {
      fs.writeFileSync(file, modded, 'utf8')
    }
  }

  return stats
}

const Identity = x => x;

const parse = (context, file, parser) => {
  const text = fs.readFileSync(path.resolve(context, file), 'utf8')
  const ast = parser.parse(text)

  return {
    lines: text.split('\n'),
    text,
    walk: fn => parser.walk(ast, fn),
  }
}

const indexBy = (f, list) => list.reduce((acc, x) => {
  const index = f(x)
  acc[index[0]] = index[1]
  return acc
}, {})

module.exports = main
